module.exports = (options) => {
  const o = options || {};

  if (!o.name) o.name = 'sid';
  if (o.httpOnly === undefined) o.httpOnly = true;
  if (!o.path) o.path = '/';

  const cookieManager = {
    readCookie: (req) => req.cookies[o.name] || null,
    getNewCookie: (value, maxAge) => {
      const cookieValue = [];

      // Add cookie value
      cookieValue.push(`${o.name}=${encodeURIComponent(value)}`);

      // Add duration
      cookieValue.push(`Max-Age=${maxAge || 0}`);

      // Set Domain
      if (o.domain) cookieValue.push(`Domain=${o.domain}`);

      // Set path
      cookieValue.push(`Path=${o.path}`);

      // Security
      if (o.httpOnly) cookieValue.push('HttpOnly');
      if (o.secure) cookieValue.push('Secure');

      return `${cookieValue.join('; ')};`;
    },
    setCookie: (res, value, maxAge) => {
      // Calculate value
      const cookieValue = cookieManager.getNewCookie(value, maxAge);

      // Write Header
      res.set('Set-Cookie', cookieValue);
    },
    clearCookie: (res) => {
      // Calculate value
      const cookieValue = cookieManager.getNewCookie('', 0);

      // Write Header
      res.set('Set-Cookie', cookieValue);
    },
  };
  return cookieManager;
};
