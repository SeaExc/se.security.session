const { expect } = require('chai');
const encryptionModule = require('encryption-se');

const sessionManagerModule = require('./sessionManager');

describe('sessionManager Tests', () => {
  it('It will reject construction if secret is missing', () => {
    expect(() => sessionManagerModule()).to.throw('Session-SE: You need to provide a secret for encryption');
  });
  it('It will read null session if cookie is not present', async () => {
    const sessionManager = sessionManagerModule({ secret: 'sdfghjuy6543qewdfg' });
    const req = { cookies: { }}
    await sessionManager.readSession(req);
    expect(req.session).to.not.exist;
  });
  it('It will read null session if cookie is present, but invalid', async () => {
    const sessionManager = sessionManagerModule({
      secret: 'sdfghjuy6543qewdfg',
      cookie: { name: 'sid' }
    });
    const req = { cookies: { sid: 'I am invalid cookie value' }};
    await sessionManager.readSession(req);
    expect(req.session).to.not.exist;
  });

  describe('Creating of a proper session', () => {
    const req = {};
    const res = { bag: { }};
    res.set = (key, value) => { res.bag[key] = value };
    before(async () => {
      const sessionManager = sessionManagerModule({
        secret: 'sdfghjuy6543qewdfg',
        cookie: { name: 'sid' },
        duration: 1
      });

      await sessionManager.createSession(req, res, { name: 'Mr User Johnson'});
    });

    it('req.user will be set', () => {
      expect(req.user).to.exist;
      expect(req.user).to.deep.equal({ name: 'Mr User Johnson'});
    });
    it('req.session will be set', () => {
      expect(req.session).to.exist;
    });
    it('req.session will have proper user object set', () => {
      expect(req.session.user).to.deep.equal(req.user);
    });
    it('Session will have correct expiration set', () => {
      expect(req.session.expiresOn).to.exist;
      expect(req.session.expiresOn - (new Date().getTime() + 1*60*1000)).to.be.below(1000);
    });
    it('Session cookie should be set', () => {
      expect(res.bag['Set-Cookie']).to.exist;
    });
    it('Session cookie should have a proper max-age', () => {
      expect(res.bag['Set-Cookie'].match(/Max-Age=60/)).to.exist;
    });
  });

  describe('It can read valid cookie', () => {
    const req = {};
    const res = { bag: { }};
    res.set = (key, value) => { res.bag[key] = value };
    const encryption = encryptionModule({ password: 'sdfghjuy6543qewdfg', passwordSalt: '123456789', iterationCount: 100000 });
    before(async () => {
      req.cookies = { sid: await encryption.encrypt(`{ "user": { "name": "Peter" }, "maxExp": ${new Date().getTime() + 60*60*1000} , "expiresOn": ${new Date().getTime() + 1000}, "id": "abc" }`) };
    });

    it('Session is properly read and saved to request', async () => {
      const sessionManager = sessionManagerModule({
        secret: 'sdfghjuy6543qewdfg',
        secretSalt: '123456789',
        iterationCount: 100000
      });
      await sessionManager.readSession(req, res);
      expect(req.session).to.exist;
      expect(req.session.user).to.exist;
      expect(req.session.user).to.deep.equal({ name: 'Peter' });
    });

    it('Session is properly extended when needed', async () => {
      const sessionManager = sessionManagerModule({
        secret: 'sdfghjuy6543qewdfg',
        secretSalt: '123456789',
        iterationCount: 100000,
        duration: 20
      });
      await sessionManager.readSession(req, res);
      expect(req.session.expiresOn).to.exist;
      expect(req.session.expiresOn - (new Date().getTime() + 20*60*1000)).to.be.below(1000);
      expect(res.bag['Set-Cookie']).to.exist;
      expect(res.bag['Set-Cookie'].match(/Max-Age=1200/)).to.exist;
    });

    it('Session is not touched if not needed to be extended', async () => {
      const session_old = {
        user: { name: 'Peter' },
        maxExp: new Date().getTime() + 60*60*1000,
        expiresOn: new Date().getTime() + 20*60*1000,
        id: "abc"
      };
      req.cookies = { sid: await encryption.encrypt(JSON.stringify(session_old))};
      const sessionManager1 = sessionManagerModule({
        secret: 'sdfghjuy6543qewdfg',
        secretSalt: '123456789',
        iterationCount: 100000,
        duration: 20
      });
      await sessionManager1.readSession(req, res);
      expect(req.session).to.deep.equal(session_old);
    });

    it('Session is extended if needed but never after maximum expiration', async () => {
      const session_old = {
        user: { name: 'Peter' },
        maxExp: new Date().getTime() + 7*60*1000, // In seven minutes
        expiresOn: new Date().getTime() + 1*60*1000, // In a minute
        id: "abc"
      };
      req.cookies = { sid: await encryption.encrypt(JSON.stringify(session_old))};
      const sessionManager1 = sessionManagerModule({
        secret: 'sdfghjuy6543qewdfg',
        secretSalt: '123456789',
        iterationCount: 100000,
        duration: 20
      });
      await sessionManager1.readSession(req, res);
      expect(req.session.expiresOn - session_old.maxExp).to.be.below(1000);
      expect(req.session.expiresOn).to.equal(req.session.maxExp);
    });

  });

  describe('Removing of expired session', () => {
    const req = {};
    const res = { bag: { }};
    res.set = (key, value) => { res.bag[key] = value };
    before(async () => {
      const encryption = encryptionModule({ password: 'sdfghjuy6543qewdfg', passwordSalt: '123456789', iterationCount: 100000 });
      req.cookies = { sid: await encryption.encrypt('{ "user": { "name": "Peter" }, "maxExp": 0, "expiresOn": 0, "id": "abc" }') };
    });

    it('Session will be destroyed if expired', async () => {
      const sessionManager = sessionManagerModule({ secret: 'sdfghjuy6543qewdfg', secretSalt: '123456789', iterationCount: 100000 });
      await sessionManager.readSession(req, res);
      expect(req.session).to.not.exist;
      expect(req.user).to.not.exist;
    })
  });

  describe('Logout will destroy session and remove cookie', () => {
    const req = {};
    const res = { bag: { }};
    res.set = (key, value) => { res.bag[key] = value };
    const encryption = encryptionModule({ password: 'sdfghjuy6543qewdfg', passwordSalt: '123456789', iterationCount: 100000 });
    before(async () => {
      req.cookies = { sid: await encryption.encrypt(`{ "user": { "name": "Peter" }, "maxExp": ${new Date().getTime() + 60*60*1000} , "expiresOn": ${new Date().getTime() + 1000}, "id": "abc" }`) };
    });

    it('Session is properly removed from a valid request', async () => {
      const sessionManager = sessionManagerModule({
        secret: 'sdfghjuy6543qewdfg',
        secretSalt: '123456789',
        iterationCount: 100000
      });
      await sessionManager.readSession(req, res);
      expect(req.session).to.exist;
      expect(req.session.user).to.exist;
      expect(req.session.user).to.deep.equal({ name: 'Peter' });

      // now logout user
      await sessionManager.logout(req, res);
      expect(req.session).to.not.exist;
      expect(req.user).to.not.exist;
      expect(res.bag['Set-Cookie']).to.exist;
      expect(res.bag['Set-Cookie'].match(/Max-Age=0/)).to.exist;
    });
  });
});
