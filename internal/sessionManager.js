const encryptionModule = require('encryption-se');
const cookieManagerModule = require('./cookieManager');
const { v4: uuid } = require('uuid');

module.exports = (options) => {
  if (!options || !options.secret)
    throw Error('Session-SE: You need to provide a secret for encryption');

  if(!options.duration)
    options.duration = 20;
  if(!options.expiration)
    options.expiration = 480;

  const encryption = encryptionModule({
    password: options.secret,
    passwordSalt: options.secretSalt || 'safawo$lfnfrn34r-fac',
    iterationCount: 100000
  });

  const cookieManager = cookieManagerModule(options.cookie);

  const sessionManager = {
    readSession: async (req, res) => {
      req.session = null;
      req.user = null;

      const value = cookieManager.readCookie(req);
      if(!value) return null;

      try {
        const sessionStr = await encryption.decrypt(value);
        const session = JSON.parse(sessionStr);

        if(session.expiresOn < new Date().getTime())
          return null;

        if(session.expiresOn - new Date() < options.duration * 60 * 1000 / 2 ||
           session.maxAge - new Date() < options.duration * 60 * 1000 / 2) {
          // Session needs to be extended
          await sessionManager.extendSession(req, res, session);
        }

        req.session = session;
        req.user = session.user;
      }
      catch(err) {
        return null;
      }
    },
    createSession: async (req, res, user) => {
      const session = {
        user,
        maxExp: new Date().getTime() + options.expiration * 60 * 1000,
        expiresOn: new Date().getTime() + options.duration * 60 * 1000,
        id: uuid()
      };

      // Do not allow session to go after maximum duration
      if (session.maxExp < session.expiresOn)
        session.expiresOn = session.maxExp;

      req.user = user;
      req.session = session;

      // Write cookie
      const enc = await encryption.encrypt(JSON.stringify(session));
      cookieManager.setCookie(res, enc, Math.ceil((session.expiresOn - new Date().getTime()) / 1000));
    },
    extendSession: async (req, res, session) => {
      session.expiresOn = new Date().getTime() + options.duration * 60 * 1000;

      // Do not allow session to go after maximum duration
      if (session.maxExp < session.expiresOn)
        session.expiresOn = session.maxExp;

      // Send new cookie
      const enc = await encryption.encrypt(JSON.stringify(session));
      cookieManager.setCookie(res, enc, Math.ceil((session.expiresOn - new Date().getTime()) / 1000))
    },
    logout: async (req, res) => {
      req.session = null;
      req.user = null;

      // Write cookie
      cookieManager.clearCookie(res);
    }
  };

  return sessionManager;
};
