const { expect } = require('chai');

const cookieManagerModule = require('./cookieManager');

describe('cookieManager Tests', () => {
  it('Can read cookie value if there is cookie', () => {
    const req = { cookies: { test1: 'Some value' } };
    const cookieManager = cookieManagerModule({ name: 'test1' });
    const value = cookieManager.readCookie(req);
    expect(value).to.equal('Some value');
  });
  it('Can read cookie value if there is no cookie present', () => {
    const req = { cookies: { } };
    const cookieManager = cookieManagerModule({ name: 'test1' });
    const value = cookieManager.readCookie(req);
    expect(value).to.equal(null);
  });
  it('It will set cookie.name to sid if not provided', () => {
    const req = { cookies: { sid: 'Some value' } };
    const cookieManager = cookieManagerModule();
    const value = cookieManager.readCookie(req);
    expect(value).to.equal('Some value');
  });
  it('It will set cookie.name to provided', () => {
    const req = { cookies: { notSid: 'Some value' } };
    const cookieManager = cookieManagerModule({ name: 'notSid' });
    const value = cookieManager.readCookie(req);
    expect(value).to.equal('Some value');
  });
  it('It will set cookie.httpOnly to true if not specified', () => {
    const cookieManager = cookieManagerModule();
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/HttpOnly/)).to.exist;
  });
  it('It will set cookie.httpOnly to true if specified true', () => {
    const cookieManager = cookieManagerModule({ httpOnly: true });
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/HttpOnly/)).to.exist;
  });
  it('It will set cookie.httpOnly to false if specified false', () => {
    const cookieManager = cookieManagerModule({ httpOnly: false });
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/HttpOnly/)).to.not.exist;
  });
  it('It will set cookie.secure to false if not specified', () => {
    const cookieManager = cookieManagerModule();
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/Secure/)).to.not.exist;
  });
  it('It will set cookie.secure to true if specified true', () => {
    const cookieManager = cookieManagerModule({ secure: true });
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/Secure/)).to.exist;
  });
  it('It will set cookie.httpOnly to false if specified false', () => {
    const cookieManager = cookieManagerModule({ secure: false });
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/Secure/)).to.not.exist;
  });
  it('It will set proper encoded value', () => {
    const cookieManager = cookieManagerModule();
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/sid=Test%20value/)).to.exist;
  });
  it('It will set proper cookie duration', () => {
    const cookieManager = cookieManagerModule();
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/Max-Age=0/)).to.exist;
  });
  it('It will set proper cookie domain if provided', () => {
    const cookieManager = cookieManagerModule({ domain: 'something.com' });
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/Domain=something.com/)).to.exist;
  });
  it('It wqill set proper path if provided', () => {
    const cookieManager = cookieManagerModule({ path: '/my-path' });
    const value = cookieManager.getNewCookie('Test value', 0);
    expect(value.match(/Path=\/my-path/)).to.exist;
  });
  it('It will set header with a new cookie', () => {
    const res = { bag: {} };
    res.set = (key, value) => { res.bag[key] = value; };
    const cookieManager = cookieManagerModule();
    cookieManager.setCookie(res, 'Some value', 0);
    expect(res.bag['Set-Cookie']).to.exist;
  });
  it('It will set header to remove cookie', () => {
    const res = { bag: {} };
    res.set = (key, value) => { res.bag[key] = value; };
    const cookieManager = cookieManagerModule();
    cookieManager.clearCookie(res);
    expect(res.bag['Set-Cookie']).to.exist;
    expect(res.bag['Set-Cookie'].match(/Max-Age=0/)).to.exist;
  });
});
