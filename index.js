const sessionManagerModule = require('./internal/sessionManager');

module.exports = (options) => {

  const sessionManager = sessionManagerModule(options);

  return async (req, res, next) => {

    req.registerUser = async (user) => {

      if(!user)
        await sessionManager.logout(req, res);

      else
        await sessionManager.createSession(req, res, user);

    };

    await sessionManager.readSession(req, res);

    next();
  }
};
